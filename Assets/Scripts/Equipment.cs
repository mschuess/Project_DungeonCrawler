﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create/Player/Equipment")]
public class Equipment : ScriptableObject {

	//public Item_Weapon mainWep, offWep;
	//public Item_Armor trinket1, trinket2;
	public Item[] equipment = new Item[4];
}
