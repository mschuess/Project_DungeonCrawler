﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleManager : MonoBehaviour {

	public GameObject[] loadPanels;
	GameObject gcgo, playerDataGO;
	GameController gc;
	PlayerData playerData;
	public string playersPath, globalPath;
	public int maxSaves = 4;
	public InputField nameInput;
	public GameObject newGameBtn;
	
	struct saveCluster {
		public PlayerStatsObject ps;
		public InventoryItems inv;
		public Equipment eq;
	}

	saveCluster[] allSaves;

	private void Awake() {
		//If we're not on the title scene, disable this script
		if (SceneManager.GetActiveScene() != SceneManager.GetSceneByName("Title")) {
			GetComponent<TitleManager>().enabled = false;
		}
	}

	// Use this for initialization
	void Start () {
		gcgo = GameObject.FindWithTag("GameController");
		gc = gcgo.GetComponent<GameController>();
		playerDataGO = GameObject.FindWithTag("PlayerData");
		playerData = playerDataGO.GetComponent<PlayerData>();
		allSaves = new saveCluster[maxSaves];

		FetchGameSaves();
	}

	public void SetGlobalSaveFiles(int n) {
		playerData.loadData_playerStats = allSaves[n].ps;
		playerData.loadData_inventory = allSaves[n].inv;
		playerData.loadData_equipment = allSaves[n].eq;
	}

	void FetchGameSaves() {
		//Find all saved players, save them to the list
		//loop through player directories
		//open +1 directory
		//Add the file containing _player.asset
		DirectoryInfo dir = new DirectoryInfo(globalPath);
		DirectoryInfo[] directories = dir.GetDirectories();
		int i = 0;
		foreach (DirectoryInfo d in directories) {
			PlayerStatsObject thisPlayer =
				Resources.Load<PlayerStatsObject>(playersPath + "/" + d.Name + "/" + d.Name + "_PlayerStats");
			Equipment thisEquipment = Resources.Load<Equipment>(playersPath + "/" + d.Name + "/" + d.Name + "_Equipment");
			InventoryItems thisInv = Resources.Load<InventoryItems>(playersPath + "/" + d.Name + "/" + d.Name + "_Inventory");

			saveCluster thisSave = new saveCluster();
			thisSave.ps = thisPlayer;
			thisSave.inv = thisInv;
			thisSave.eq = thisEquipment;
			allSaves[i] = thisSave;

			UpdateUI(i, thisPlayer, thisEquipment);
			i++;
			if (i >= maxSaves) {
				newGameBtn.GetComponent<Button>().interactable = false;
				break;
			}
		}

		//Remove the details from any remaining empty panels
		if (i < maxSaves)
			for (int x = i; x < maxSaves; x++)
				foreach (Transform child in loadPanels[x].transform)
					child.gameObject.SetActive(false);
	}

	void UpdateUI(int i, PlayerStatsObject thisPlayer, Equipment thisEquipment) {
		Text statsText = loadPanels[i].transform.GetChild(0).transform.GetComponent<Text>();
		Image item1 = loadPanels[i].transform.GetChild(1).GetComponent<Image>();
		Image item2 = loadPanels[i].transform.GetChild(2).GetComponent<Image>();
		Image item3 = loadPanels[i].transform.GetChild(3).GetComponent<Image>();
		Image item4 = loadPanels[i].transform.GetChild(4).GetComponent<Image>();
		statsText.text = "" + thisPlayer.playerName
						+ "\nHP: " + thisPlayer.playerHP
						+ "\nDefense: " + thisPlayer.defense
						+ "\nCrit Rating: " + thisPlayer.critical
						+ "\nStrength: " + thisPlayer.strength
						+ "\nIntelligence: " + thisPlayer.intelligence;

		//fuck it
		try {
			item1.sprite = thisEquipment.equipment[0].thumbnail;
		} catch { Debug.Log("Equipment sprite 1 empty"); }

		try {
			item2.sprite = thisEquipment.equipment[1].thumbnail;
		}catch { Debug.Log("Equipment sprite 2 empty"); }

		try {
			item3.sprite = thisEquipment.equipment[2].thumbnail;
		}catch { Debug.Log("Equipment sprite 3 empty"); }

		try {
			item4.sprite = thisEquipment.equipment[3].thumbnail;
		}catch { Debug.Log("Equipment sprite 4 empty"); }
	}

	public void UpdateNameInput() {
		name = nameInput.text;
		gc.NewGame(name);
	}
}
