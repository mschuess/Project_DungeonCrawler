﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerNew : MonoBehaviour {

	public float Speed = 5f, JumpHeight = 2f, Gravity = -9.81f, GroundDistance = 0.2f, DashDistance = 5f;
	public bool canJump;
	public LayerMask Ground;
	public Vector3 Drag;
	

	CharacterController _controller;
	Vector3 _velocity;
	bool _isGrounded = true;
	Transform _groundChecker;
	Animator anim;
	Attack attack;
	enum Actions { moving, attacking }
	Actions actionState;

	void Start() {
		_controller = GetComponent<CharacterController>();
		_groundChecker = transform.GetChild(0);
		anim = transform.GetChild(1).GetComponent<Animator>();
		attack = GetComponent<Attack>();
	}

	void Update() {
		if (actionState == Actions.moving) {
			_isGrounded = Physics.CheckSphere(_groundChecker.position, GroundDistance, Ground, QueryTriggerInteraction.Ignore);
			if (_isGrounded && _velocity.y < 0)
				_velocity.y = 0f;
			Run();
			if (Input.GetKeyDown(KeyCode.Space) && _isGrounded)
				Jump();
			if (anim.GetBool("is_in_air") && !_isGrounded) anim.SetBool("is_in_air", false);
			_velocity.y += Gravity * Time.deltaTime;
			_velocity.x /= 1 + Drag.x * Time.deltaTime;
			_velocity.y /= 1 + Drag.y * Time.deltaTime;
			_velocity.z /= 1 + Drag.z * Time.deltaTime;
			_controller.Move(_velocity * Time.deltaTime);
		}else if(actionState == Actions.attacking) {
			
			
		}

		if (Input.GetKeyDown(KeyCode.Mouse1)) {
			Debug.Log("Mouse 1 pressed");
			attack.DrawHover();
			//actionState = Actions.attacking;
			attack.Cast();
		}
	}

	void Jump() {
		_velocity.y += Mathf.Sqrt(JumpHeight * -2f * Gravity);
		anim.SetBool("is_in_air", true);
	}

	void Run() {
		Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		_controller.Move(move * Time.deltaTime * Speed);
		if (move != Vector3.zero) {
			transform.forward = move;
			anim.SetBool("run", true);
		}
		else {
			anim.SetBool("run", false);
		}
	}
}
