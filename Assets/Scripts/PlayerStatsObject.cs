﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create/Player/Player Stats")]
public class PlayerStatsObject : ScriptableObject {

	[Header("Main Player")]
	public string playerName;
	//[SerializeField]
	public int playerHP = 100, defense = 10, critical = 10, strength = 10, intelligence = 10;

}
