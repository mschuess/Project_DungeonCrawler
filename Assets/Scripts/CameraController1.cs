﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController1 : MonoBehaviour {

	public float offsetY, offsetZ;
	GameObject player;
	Vector3 offset;
	// Use this for initialization
	void Start () {
		player = GameObject.FindWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		offset = new Vector3(player.transform.position.x, player.transform.position.y + offsetY, player.transform.position.z + offsetZ);
		transform.position = offset;
		transform.LookAt(player.transform.position);
	}
}
