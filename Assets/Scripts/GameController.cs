﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class GameController : MonoBehaviour {

	public bool paused, saveBeforeQuit;
	GameObject player, playerDataGO;
	public GameObject pausePanel;
	InventoryManager invMgr;
	PlayerData playerData;
	PlayerStats ps;


	// Use this for initialization
	void Start () {
		player = GameObject.FindWithTag("Player");
		invMgr = player.GetComponent<InventoryManager>();
		playerDataGO = GameObject.FindWithTag("PlayerData");
		playerData = playerDataGO.GetComponent<PlayerData>();
		//Just so we don't get an error at the title scene
		try {
			ps = player.GetComponent<PlayerStats>();
		}catch {
			Debug.Log("Warning: No player stats attached to player.");
		}
		//pausePanel.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			Pause();
			pausePanel.SetActive(paused);
		}
	}

	public void Pause() {
		paused = !paused;
	}

	public void NewGame(string name) {
		//Create player data for new character
		DirectoryInfo newDir = Directory.CreateDirectory("Assets/Resources/SaveFiles/Players/" + name);
		CreateInventory(name);
		CreateEquipment(name);
		CreatePlayerStats(name);
		//Set the global player data
		DirectoryInfo dir = new DirectoryInfo("Assets/Resources/SaveFiles/Players");
		playerData.loadData_playerStats = Resources.Load<PlayerStatsObject>("SaveFiles/Players/" + name + "/" + name + "_PlayerStats");
		playerData.loadData_inventory = Resources.Load<InventoryItems>("SaveFiles/Players/" + name + "/" + name + "_Inventory");
		playerData.loadData_equipment = Resources.Load<Equipment>("SaveFiles/Players/" + name + "/" + name + "_Equipment");
		playerData.loadData_playerStats.playerName = name;
		//Load the main scene
		LoadScene("Main");
	}

	public void SaveGame() {
		Debug.Log("=== Saving Game ===");
		//Save inventory
		InventoryManager invMgr = player.GetComponent<InventoryManager>();
		invMgr.SaveInventory();
		EquipmentManager eqMgr = player.GetComponent<EquipmentManager>();
		eqMgr.SaveEquipment();
		PlayerStats ps = player.GetComponent<PlayerStats>();
		ps.SaveStats();
	}

	public void QuitGame() {
		if (saveBeforeQuit) SaveGame();
	}

	public void LoadScene(string sceneName) {
		SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
	}

	public static InventoryItems CreateInventory(string name) {
		InventoryItems asset = ScriptableObject.CreateInstance<InventoryItems>();
		AssetDatabase.CreateAsset(asset, "Assets/Resources/SaveFiles/Players/" + name + "/" + name + "_Inventory.asset");
		AssetDatabase.SaveAssets();
		return asset;
	}

	public static Equipment CreateEquipment(string name) {
		Equipment asset = ScriptableObject.CreateInstance<Equipment>();
		AssetDatabase.CreateAsset(asset, "Assets/Resources/SaveFiles/Players/" + name + "/" + name + "_Equipment.asset");
		AssetDatabase.SaveAssets();
		return asset;
	}

	public static PlayerStatsObject CreatePlayerStats(string name) {
		PlayerStatsObject asset = ScriptableObject.CreateInstance<PlayerStatsObject>();
		AssetDatabase.CreateAsset(asset, "Assets/Resources/SaveFiles/Players/" + name + "/" + name + "_PlayerStats.asset");
		AssetDatabase.SaveAssets();
		return asset;
	}
}
