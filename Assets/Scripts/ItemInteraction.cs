﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemInteraction : MonoBehaviour {

	public GameObject proximityDetails, thisObject;
	public Text itemNameText;
	public Image itemSprite;
	public bool onItem;

	public Item thisItem;
	InventoryManager invMgr;

	// Use this for initialization
	void Start () {
		proximityDetails.SetActive(false);
		invMgr = GameObject.FindWithTag("Player").GetComponent<InventoryManager>();
	}
	
	// Update is called once per frame
	void Update () {
		if(onItem && Input.GetKeyDown(KeyCode.F)) {
			UseItem();
		}
	}

	public void UseItem() {
		//If item.type == ...
		if(thisItem != null && onItem) {
			if (thisItem.canPickup) {
				print("IT WORKED");
				invMgr.AddItem(thisItem);
				thisObject.SetActive(false);
				invMgr.SetInventoryUI();
				onItem = false;
				proximityDetails.SetActive(false);
			}
		}
	}

	public void OnTriggerEnter(Collider other) {
		if(other.gameObject.GetComponent<ItemObject>() != null) {
			thisItem = other.gameObject.GetComponent<ItemObject>().itemObj;
			thisObject = other.gameObject;
			proximityDetails.SetActive(true);
			Vector3 newPos = other.transform.position;
			newPos.y += 8f;
			proximityDetails.transform.position = newPos;
			onItem = true;
			itemNameText.text = thisItem.itemName;
			itemSprite.sprite = thisItem.thumbnail;
		}
	}

	public void OnTriggerExit(Collider other) {
		if (other.gameObject.GetComponent<ItemObject>() != null) {
			thisItem = other.gameObject.GetComponent<ItemObject>().itemObj;
			proximityDetails.SetActive(false);
			onItem = false;
		}
	}
}
