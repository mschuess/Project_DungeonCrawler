﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour {

	public PlayerStatsObject loadData_playerStats;
	public InventoryItems loadData_inventory;
	public Equipment loadData_equipment;

	private void Awake() {
		DontDestroyOnLoad(gameObject);
	}
}
