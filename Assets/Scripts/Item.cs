﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : ScriptableObject {
	public string itemName, itemType, rarity;
	public int sellPrice, buyPrice, rarityPercent;
	public Sprite thumbnail;
	public bool isUnique, isQuestItem, isStackable, destroyOnUse, canPickup;
	public GameObject prefab;
}
