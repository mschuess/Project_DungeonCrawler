﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create/Items/Armor")]
public class Item_Armor : Item {
	public int defense, critical, strength, intelligence;
}
