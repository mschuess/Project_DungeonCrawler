﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class EquipmentManager : MonoBehaviour {
	public Image[] equipmentSlots;// mainHand, offHand, trinket1, trinket2;
	public Text nameText, hpText, defenseText, critText, strText, intText, wpnLvlText;
	GameObject playerDataGO, player;
	PlayerData playerData;
	GameController gc;
	InventoryManager invMgr;
	Equipment eq;
	PlayerStats ps;
	public Equipment fallbackEq;
	public GameObject detailsPanel;
	public Item[] equipment;

	// Use this for initialization
	void Start () {
		gc = GameObject.FindWithTag("GameController").GetComponent<GameController>();
		playerDataGO = GameObject.FindWithTag("PlayerData");
		playerData = playerDataGO.GetComponent<PlayerData>();
		player = GameObject.FindWithTag("Player");
		ps = player.GetComponent<PlayerStats>();
		try {
			eq = playerData.loadData_equipment;
		}catch {
			eq = fallbackEq;
			Debug.Log("Unable to load selected player's equipment. Using default player.");
		}
		equipment = new Item[4];
		eq.equipment.CopyTo(equipment, 0);

		invMgr = GetComponent<InventoryManager>();

		UpdateEquipmentUI();
	}

	public void EquipItem() {
		string itemType = invMgr.selectedItem.itemType;
		//item 1 is the selected item to be equipped
		//item 2 is the equipment item to be replaced
		Item item1 = invMgr.selectedItem, item2;
		bool shouldRemove = true;
		switch (itemType) {
			case "weapon":
				Item_Weapon thisWeapon = (Item_Weapon)item1;
					
				if (thisWeapon.oneHand && !thisWeapon.offHand) {
					if(equipment[0] != null) {
						Item_Weapon tempWep = (Item_Weapon)equipment[0];
						if (!tempWep.oneHand) {
							item2 = equipment[0];
							invMgr.items.Add(item2);
							equipment[0] = item1;
						}else{
							if (equipment[1] != null) {
								item2 = equipment[1];
								invMgr.items.Add(item2);
							}
							equipment[1] = item1;
						}
					}else {
						equipment[0] = item1;
					}
				}else if (!thisWeapon.oneHand && !thisWeapon.offHand) {
					if(equipment[0] != null) {
						item2 = equipment[0];
						invMgr.items.Add(item2);
					}
					if(equipment[1] != null) {
						item2 = equipment[1];
						invMgr.items.Add(item2);
						equipment[1] = null;
					}
					equipment[0] = item1;
				}else if (thisWeapon.offHand) {
					if (equipment[0] != null) {
						Item_Weapon tempWep = (Item_Weapon)equipment[0];
						if (!tempWep.oneHand) {
							item2 = equipment[0];
							invMgr.items.Add(item2);
							equipment[0] = null;
						}
					}
					if(equipment[1] != null) {
						item2 = equipment[1];
						invMgr.items.Add(item2);
					}
					equipment[1] = item1;
				}
				else { shouldRemove = false; }
				break;
			case "armor":
				if(equipment[2] != null && equipment[3] != null) {
					item2 = equipment[3];
					invMgr.items.Add(item2);
					equipment[3] = item1;
				}else if (equipment[2] == null) {
					equipment[2] = item1;
				}else if (equipment[2] != null && equipment[3] == null) {
					equipment[3] = item1;
				}
				else { shouldRemove = false; }
				break;
			default:
				print("No item type assigned for this item.");
				shouldRemove = false;
				break;
		}
		if(shouldRemove)
			invMgr.RemoveSelected();

		detailsPanel.SetActive(false);
		invMgr.SetInventoryUI();
		UpdateEquipmentUI();
		CalculateStats();
	}

	public void UnequipItem(Item item) {

	}

	public void UpdateEquipmentUI() {
		int i = 0;
		foreach(Item item in equipment) {
			if (item != null) equipmentSlots[i].sprite = item.thumbnail;
			else equipmentSlots[i].sprite = null;
			i++;
		}
		nameText.text = ps.PlayerName;
		if (equipment[0] != null) {
			Item_Weapon primaryWep = (Item_Weapon)equipment[0];
			wpnLvlText.text = primaryWep.itemName + ": level " + primaryWep.level;
		}
		else {
			wpnLvlText.text = "";
		}
		hpText.text = "HP: " + ps.PlayerHP;
		defenseText.text = "Defense: " + ps.Defense;
		critText.text = "Crit: " + ps.Critical;
		strText.text = "Strength: " + ps.Strength;
		intText.text = "Intelligence: " + ps.Intelligence;
	}

	public void CalculateStats() {
		ps.Defense = 10;
		ps.Critical = 10;
		ps.Strength = 10;
		ps.Intelligence = 10;
		foreach (Item item in equipment) {
			if (item == null) continue;
			if (item.itemType == "weapon") {
				Item_Weapon thisWep = (Item_Weapon)item;
				ps.Defense += thisWep.defense;
				ps.Critical += thisWep.critical;
				ps.Strength += thisWep.strength;
				ps.Intelligence += thisWep.intelligence;
			}
			else if(item.itemType == "armor") {
				Item_Armor thisArmor = (Item_Armor)item;
				ps.Defense += thisArmor.defense;
				ps.Critical += thisArmor.critical;
				ps.Strength += thisArmor.strength;
				ps.Intelligence += thisArmor.intelligence;
			}
		}
	}

	public void SaveEquipment() {
		//playerData.loadData_equipment.equipment = equipment;
		equipment.CopyTo(eq.equipment, 0);
		AssetDatabase.SaveAssets();
	}
}
