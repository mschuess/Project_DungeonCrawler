﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEngine.EventSystems;

public class InventoryManager : MonoBehaviour {

	public GameObject inventoryPanel, invSlotsParent, equipmentParent, detailsPanel, playerDataGO;
	InventoryItems inventoryItems;
	PlayerData playerData;
	public InventoryItems fallbackInventory;
	public PlayerStatsObject playerStatsObj;
	int previousSlot, slotNum;
	bool itemIsSelected;
	public Item selectedItem;

	public List<Item> items;
	GameController gc;

	// Use this for initialization
	void Start () {
		playerDataGO = GameObject.FindWithTag("PlayerData");
		playerData = playerDataGO.GetComponent<PlayerData>();
		try {
			gc = GameObject.FindWithTag("GameController").GetComponent<GameController>();
			inventoryItems = playerData.loadData_inventory;
			items.AddRange(inventoryItems.items);
		}
		catch {
			Debug.Log("Unable to load selected player's Inventory. Using default player.");
			items = fallbackInventory.items;
		}
		inventoryPanel.SetActive(false);
		detailsPanel.SetActive(false);
		SetInventoryUI();
		SetEquipmentPage();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.I)) {
			inventoryPanel.SetActive(!inventoryPanel.activeSelf);
		}
	}

	public void SetInventoryUI() {
		int i = 0;
		foreach (Transform child in invSlotsParent.transform) {
			if (i < items.Count && items[i] != null) {
				print("Setting inv slot");
				//print("==========   " + i + " == " + items.Count);
				child.gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 1);
				child.GetComponent<Image>().sprite = items[i].thumbnail;
				child.GetChild(0).transform.GetComponent<Text>().text = items[i].itemName;
				child.gameObject.GetComponent<Button>().interactable = true;
				i++;
			}else {
				print("Inv slot to default");
				child.gameObject.GetComponent<Image>().sprite = null;
				child.gameObject.GetComponent<Image>().color = new Color(1, 1, 1, .5f);
				child.GetChild(0).transform.GetComponent<Text>().text = "";
				child.gameObject.GetComponent<Button>().interactable = false;
			}
		}
	}

	public void OnClickDetails() {
		//display picture, name, stats			<- in that order
		//Figure out the slot number
		slotNum = 0;
		foreach(Transform child in invSlotsParent.transform) {
			if (slotNum < items.Count) {
				print(EventSystem.current.currentSelectedGameObject.name);
				if (child == EventSystem.current.currentSelectedGameObject.transform) {
					break;
				}
				slotNum++;
			}else return;
		}

		//print("slotnum: " + slotNum + " -- " + "capacity: " + items.Capacity + "count: " + items.Count);

		selectedItem = items[slotNum];
		detailsPanel.SetActive(previousSlot == slotNum && detailsPanel.activeSelf ? false : true);
		itemIsSelected = detailsPanel.activeSelf;
		previousSlot = slotNum;
		detailsPanel.transform.GetChild(0).GetComponent<Image>().sprite = items[slotNum].thumbnail;
		detailsPanel.transform.GetChild(1).GetComponent<Text>().text = items[slotNum].itemName;
		detailsPanel.transform.GetChild(2).GetComponent<Text>().text = "Item Type: " + items[slotNum].itemType;

	}

	void SetEquipmentPage() {
		equipmentParent.transform.GetChild(0).GetComponent<Text>().text = playerStatsObj.playerName;
		//equipmentParent.transform.GetChild(1).GetComponent<Text>().text = "Level " + playerStatsObj.playerLevel;
		//equipmentParent.transform.GetChild(2).GetComponent<Text>().text = "XP: " + playerStatsObj.playerXP;
	}

	public static InventoryItems Create() {
		PlayerStats ps = GameObject.FindWithTag("Player").GetComponent<PlayerStats>();
		InventoryItems asset = ScriptableObject.CreateInstance<InventoryItems>();
		AssetDatabase.CreateAsset(asset, "Assets/Resources/SaveFiles/Players/" + ps.PlayerName + "_Inventory.asset");
		AssetDatabase.SaveAssets();
		return asset;
	}

	public void SaveInventory() {
		//playerData.loadData_inventory.items = items;
		inventoryItems.items.Clear();
		inventoryItems.items.AddRange(items);
		AssetDatabase.SaveAssets();
	}

	public void AddItem(Item item) {
		items.Add(item);
	}

	public void RemoveItem(Item item) {
		items.Remove(item);
	}

	public void RemoveSelected() {
		if (itemIsSelected) {
			print("Deleting slot #" + slotNum);
			items.Remove(items[slotNum]);
			slotNum = 0;
			SetInventoryUI();
			detailsPanel.SetActive(false);
		}
	}
}
