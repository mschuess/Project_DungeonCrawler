﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create/Items/Weapon")]
public class Item_Weapon : Item {
	public int manaCost, energyCost, level;
	public float coolDown;
	public bool oneHand, offHand;
	//stats
	public int defense, critical, strength, intelligence;
}
