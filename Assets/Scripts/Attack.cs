﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Attack : MonoBehaviour {

	float currAbility, previousAbility, maxAbilities = 5;
	public Button[] actionButtons;

	PlayerControllerNew pc;
	enum abilities { dash, aoe }

	// Use this for initialization
	void Start () {
		pc = GetComponent<PlayerControllerNew>();
		SelectAbility();
	}
	
	// Update is called once per frame
	void Update () {
		previousAbility = currAbility;
		if (Input.GetKeyDown(KeyCode.Alpha1)) currAbility = 0;
		if (Input.GetKeyDown(KeyCode.Alpha2)) currAbility = 1;
		if (Input.GetKeyDown(KeyCode.Alpha3)) currAbility = 2;
		if (Input.GetKeyDown(KeyCode.Alpha4)) currAbility = 3;
		if (Input.GetKeyDown(KeyCode.Alpha5)) currAbility = 4;

		if (Input.GetAxis("Mouse ScrollWheel") > 0f) {
			if (currAbility >= maxAbilities - 1)
				currAbility = 0;
			else
				currAbility++;

		}
		if (Input.GetAxis("Mouse ScrollWheel") < 0f) {
			if (currAbility <= 0)
				currAbility = maxAbilities - 1;
			else
				currAbility--;
		}

		if (currAbility != previousAbility) SelectAbility();
	}

	public void Cast() {

	}

	public void DrawHover() {

	}

	public void SelectAbility() {
		int i = 0;
		foreach (Button button in actionButtons) {
			print("i: " + i + " -- currAbility: " + currAbility);
			Color col;
			if (i == currAbility) col = new Color(255, 255, 255, 1);
			else col = new Color(255, 0, 0, 55);

			ColorBlock colorBlock = new ColorBlock();
			colorBlock.colorMultiplier = 1;
			colorBlock.normalColor = col;
			button.colors = colorBlock;
			i++;
		}
	}
}
