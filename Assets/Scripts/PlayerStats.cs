﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerStats : MonoBehaviour {
	
	[SerializeField]
	string playerName;
	[SerializeField]
	int playerHP = 100, defense = 10, critical = 10, strength = 10, intelligence = 10;

	public string PlayerName{ get{ return playerName; } set { playerName = value; } }
	public int PlayerHP { get { return playerHP; } set { playerHP = value; } }
	public int Defense { get { return defense; } set { defense = value; } }
	public int Critical { get { return critical; } set { critical = value; } }
	public int Strength { get { return strength; } set { strength = value; } }
	public int Intelligence { get { return intelligence; } set { intelligence = value; } }


	GameObject gc, pd;
	PlayerData playerData;

	private void Start() {
		gc = GameObject.FindWithTag("GameController");
		pd = GameObject.FindWithTag("PlayerData");
		playerData = pd.GetComponent<PlayerData>();
		LoadStats();
	}

	void LoadStats() {
		PlayerStatsObject pso = playerData.loadData_playerStats;
		playerName = pso.playerName;
		playerHP = pso.playerHP;
		defense = pso.defense;
		critical = pso.critical;
		strength = pso.strength;
		intelligence = pso.intelligence;
	}

	public void SaveStats() {
		//For reference:
		//int playerHP = 50, defense, critical, strength, intelligence;
		PlayerStatsObject psObject = playerData.loadData_playerStats;
		psObject.playerName = PlayerName;
		psObject.playerHP = PlayerHP;
		psObject.defense = Defense;
		psObject.critical = Critical;
		psObject.strength = Strength;
		psObject.intelligence = Intelligence;
		playerData.loadData_playerStats = psObject;
		AssetDatabase.SaveAssets();
	}
}

