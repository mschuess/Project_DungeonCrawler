﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create/Player/Inventory")]
public class InventoryItems : ScriptableObject {
	public List<Item> items = new List<Item>();
}
