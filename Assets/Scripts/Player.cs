﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create/Player/New Player")]
public class Player : ScriptableObject {

	public InventoryItems inventory;
	public Equipment equipment;
	public PlayerStatsObject playerStats;
}
