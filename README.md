# Project_DungeonCrawler

A rogue-like dungeon crawler with MOBA free style combat mechanics. This project is to meet the requirements for the ITIN Capstone course at the University of Nebraska at Omaha and is a personal project of mine. 

Future tasks:

Inventory System
-Defining items (currently only a weapon class)
-Player equipment

Player
-Stats
-Progress
-Combat
    The combat for this game is meant to be fast paced MOBA style point-and-click combat with a free movement system (WASD). 
-Items (drop, pickup, equip)
    Weapons, armor, and food are the only items that can be found in game. The character does not level up, but rather levels up their weapon. Armor is paired with weapon types by providing damage bonuses. Weapons typically level up once per floor. If the player finds a new weapon and ditches, for example, a level 5 weapon, then the player has to level up the new weapon from level 1. As weapons are leveled, the player unlocks new abilities for it (Max. of 6 abilities per weapon; can be less). 

Dungeon generation
-Rooms
    Each room is generated based on the theme of the current floor. All rooms on a single floor will share difficulty. Rooms can be classified based on what they spawn.
-Floors
    Each floor can be explored as much as the player likes. Each room generates up to four possible doors. Upon killing a boss, the floor is completed and the player moves on to the next floor. Each room that is discovered is added to the player’s map. To prevent players from never leaving floor 1, there is also an XP limit. As the player advances floors, the XP limit is raised as enemies provide more XP. Bosses provide the max XP of the floor. 
-Enemies
    Each floor contains three enemy difficulties and a boss. Each enemy difficulty includes sub-enemy types (melee, ranged, magic). Enemies will gradually deal more damage per floor while the difficulty calculations remain the same (numOfEnemies+(difficulty*floorNum)). New enemies are introduced every 5 levels. Each floor has a new boss. 

Navigation
The map will be generated in realtime

